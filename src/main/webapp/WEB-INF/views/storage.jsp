<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="false" %>


<head>
    <title>Trader Page</title>
</head>
<body>

<!-- Tab links -->

<div class="tab">
    <button class="tablinks" onclick="openEDO(event, 'FullStorage')" id="BtnFullStorage">Склад</button>
    <button class="tablinks" onclick="openEDO(event, 'Directory')" id="BtnDirectory">Справочник</button>
    <button class="tablinks" onclick="openEDO(event, 'Coming')" id="BtnComing">Приход</button>
    <button class="tablinks" onclick="openEDO(event, 'Reports')" id="BtnReports">Отчёты</button>
</div>
<body>
<!-- Tab content -->
<div id="FullStorage" class="tabcontent">
    <table class="table" style="width: 300px;">

        <tr>
            <td>
                <div class="table" style="width: 550px; height: 188px; word-wrap: break-word;">
                    <br size="40">
                    <h3>Торговля и склад v.1.0.0</h3>
                    <p><span id="info1-item"></span></p>
                    <p><span id="info2-item"></span></p>
                    <p><span id="info3-item"></span></p>
                </div>
            </td>

            <td>
                <br size="40">
                <h3>Собрано компютеров <span id="info1-sbor"></span></h3>
                <div class="table" style="width: 350px; height: 188px; word-wrap: break-word; font-size: 8px;">

                    <table id="thetableinfo" class="table table-hover table-fixed table-sm"
                           onselectstart="return false" readonly="readonly">
                        <tbody>

                        </tbody>
                    </table>
                    <%--      <p><br size="40"><span id="info1-sbor"></span></p>
                      <p><span id="info2-sbor"></span></p> --%>
                    <p><span id="info3-sbor"></span></p>

                </div>

                <!-- Правая ячейка таблицы от инфо   -->
            </td>
        </tr>
    </table>


    <c:if test="${!empty listTraders}">
        <table class="table" style="width: 1500px;">
            <tr>
                <td>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3>
                                Наличие комплектующих
                            </h3>
                            <p>Полное наличие комплектующих на складе.</p>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>

                    <div class="container">
                        <div class="row">
                            <table id="thetable1" class="table table-hover table-fixed table-sm"
                                   onselectstart="return false" readonly="readonly">
                                <thead>
                                <tr>
                                    <th class="col-xs-1">код</th>
                                    <th class="col-xs-5">наименование</th>
                                    <th class="col-xs-1">кол</th>
                                    <th class="col-xs-1">цена</th>
                                    <th class="col-xs-2">категория</th>
                                    <th class="col-xs-1">edit</th>
                                    <th class="col-xs-1">delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${listTraders}" var="storage">
                                    <tr>
                                        <td class="col-xs-1">${storage.id}</td>
                                        <td class="col-xs-5">${storage.name}</td>
                                        <td class="col-xs-1">${storage.quantity}</td>
                                        <td class="col-xs-1">${storage.price}</td>
                                        <td class="col-xs-2">${storage.category.categoryName}</td>
                                        <td class="col-xs-1"><a onclick="goToEdit(${storage.id})"
                                                                href="<c:url value='/edit/${storage.id}' />">Edit</a>
                                        </td>
                                        <td class="col-xs-1"><a
                                                href="<c:url value='/remove/${storage.id}' />">Delete</a></td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </td>
                <td>
                        <%--      <c:if test="${!empty listMoving}">  --%>
                    <div class="container" id="DivThetable2">
                        <div class="row">
                            <div class="panel panel-default">

                                <table id="thetable2" class="table table-hover table-fixed table-sm"
                                       onselectstart="return false" readonly="readonly">
                                    <thead>
                                    <tr>
                                        <th class="col-xs-1">код</th>
                                        <th class="col-xs-5">наименование</th>
                                        <th class="col-xs-1">кол</th>
                                        <th class="col-xs-1">цена</th>
                                        <th class="col-xs-2">категория</th>
                                        <th class="col-xs-1">edit</th>
                                        <th class="col-xs-1">delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:if test="${!empty listMoving}">
                                        <c:forEach items="${listMoving}" var="assembly">
                                            <tr>
                                                <td class="col-xs-1">${assembly.id}</td>
                                                <td class="col-xs-5">${assembly.name}</td>
                                                <td class="col-xs-1">${assembly.quantity}</td>
                                                <td class="col-xs-1">${assembly.price}</td>
                                                <td class="col-xs-2">${assembly.category.categoryName}</td>
                                                <!--   <td class="col-xs-1"><a id="target2" href="<c:url value='/edit/${assembly.id}' />">Edit</a></td>  -->

                                                <td class="col-xs-1"><a onclick="goToEdit(${assembly.id})"
                                                                        href="<c:url value='/edit/${assembly.id}'/>">Edit</a>
                                                </td>
                                                <td class="col-xs-1"><a href="<c:url value='/remove/${assembly.id}'/>">Delete</a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </c:if>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                        <%--   </c:if> --%>
                </td>
            </tr>
        </table>
    </c:if>
</div>

<div id="Directory" class="tabcontent">
    <table class="table" style="width: 800px;">
        <tr>
            <td>
                <div class="table" style="width: 550px; height: 100px;">
                    <br>
                    <h3>Торговля и склад v.1.0.0</h3>
                    <h1>
                        Добавить комплектующее
                    </h1>
                </div>
            </td>
            <td>
                <!-- Правая ячейка таблицы от инфо   -->

            </td>
        </tr>
        <tr>
    </table>

    <table>
        <tr>
            <td valign="top">

                <table>
                    <tbody>
                    <tr>
                        <td>
                            <c:url var="addAction" value="/storage/add"></c:url>


                            <form:form action="${addAction}" commandName="storage">


                            <c:if test="${!empty storage.name}">
                    <tr>
                        <td>
                            <form:label path="id">
                                <spring:message text="код"/>
                            </form:label>
                        </td>
                        <td>
                            <form:input path="id" readonly="true" size="8" disabled="true"/>
                            <form:hidden path="id"/>
                        </td>
                    </tr>
                    </c:if>
                    <tr>
                        <td>
                            <form:label path="name">
                                <spring:message text="Наименование"/>
                            </form:label>
                        </td>
                        <td>
                            <form:input path="name"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <form:label path="quantity">
                                <spring:message text="Количество"/>
                            </form:label>
                        </td>
                        <td>
                            <form:input path="quantity"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <form:label path="price">
                                <spring:message text="Цена"/>
                            </form:label>
                        </td>
                        <td>
                            <form:input path="price"/>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <form:label path="category.categoryName">
                                <spring:message text="Категория"/>
                            </form:label>
                        </td>
                        <td>

                            <form:select name="storage" path="category.categoryId" id="SelectMyLove"
                                         cssStyle="width: 150px;">
                                <c:forEach items="${categories}" var="category">
                                    <option
                                            <c:if test="${category.categoryId eq storage.category.categoryId}">selected="selected"</c:if>
                                            value="${category.categoryId}">${category.categoryName}
                                    </option>

                                </c:forEach>
                            </form:select>


                        </td>
                    </tr>


                    <tr>
                        <td colspan="2">
                            <c:if test="${!empty storage.name}">
                                <input type="submit"
                                       value="<spring:message text="Edit Trader"/>"/>
                            </c:if>
                            <c:if test="${empty storage.name}">
                                <input type="submit"
                                       value="<spring:message text="Add Trader"/>"/>
                            </c:if>
                        </td>
                    </tr>
                    </form:form>

                    </tbody>
                </table>
            </td>

            <td colspan="4">
                <h2>нужно ка-то отступить, пока умею так )))</h2>
            </td>
            <td>
                <table>
                    <h2>Справочник "Категории"</h2>


                    <c:url var="addAction" value="/addCategory"></c:url>

                    <form:form method="POST" commandName="storage" action="${addAction}" modelAttribute="category">
                        <c:out value="${category.categoryName}"/>


                        <tr>
                            <td><form:label path="categoryId">Код:</form:label></td>
                            <td><form:input path="categoryId" value="${categoryId}" readonly="true"/></td>
                        </tr>
                        <tr>
                            <td><form:label path="categoryName">Наименование:</form:label></td>
                            <td><form:input path="categoryName" value="${categoryName}"/></td>
                        </tr>

                        <td>&nbsp;</td>
                        <c:if test="${!empty categories}">
                            <td><input type="submit"
                                       value="<spring:message text="Save Category"/>"/></td>
                        </c:if>
                        <c:if test="${empty categories}">
                            <td><input type="submit"
                                       value="<spring:message text="Add Category"/>"/></td>
                        </c:if>


                    </form:form>


                    <c:if test="${!empty categories}">

                    <tr>
                        <th>код</th>
                        <th>наименование</th>
                        <th>опции</th>
                    </tr>

                </table>
                <div style="height: 200px; overflow: auto; border-spacing: 9px">
                    <table class="table table-bordered table-striped">
                        <tbody>

                        <c:forEach items="${categories}" var="category">
                            <tr>
                                <td><c:out value="${category.categoryId}"/></td>
                                <td><c:out value="${category.categoryName}"/></td>
                                <td align="center"><a
                                        href="<c:url value='/editCategory/${category.categoryId}'/>">Edit</a> | <a
                                        href="<c:url value='/deleteCategory/${category.categoryId}' />">Delete</a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>

                        </c:if>

                    </table>
                </div>
            </td>
        </tr>
    </table>


    <!-- Правая ячейка таблицы от инфо   -->


    <table align="left">

        <h3>Справочник</h3>
        <p>Объекты аналитического учета.</p>
        <c:if test="${!empty getDrug}">
            <p><c:out value="${getDrug}"/></p>
        </c:if>
        <c:if test="${!empty listTraders}">
            <div class="container">
                <div class="row">
                    <table id="thetableDict" class="table table-hover table-fixed table-sm"
                           onselectstart="return false"
                           readonly="readonly">
                        <thead>
                        <tr>
                            <th class="col-xs-1">код</th>
                            <th class="col-xs-4">наименование</th>
                            <th class="col-xs-1">кол</th>
                            <th class="col-xs-2">цена</th>
                            <th class="col-xs-2">категория</th>
                            <th class="col-xs-1">edit</th>
                            <th class="col-xs-1">delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${listTraders}" var="storage">
                            <tr>
                                <td class="col-xs-1">${storage.id}</td>
                                <td class="col-xs-4">${storage.name}</td>
                                <td class="col-xs-1">${storage.quantity}</td>
                                <td class="col-xs-2">${storage.price}</td>
                                <td class="col-xs-2">${storage.category.categoryName}</td>
                                <!--<td class="col-xs-1" id="target3"><a href="<c:url value='/edit/${storage.id}' />">Edit</a></td>  -->
                                <td class="col-xs-1" id="target3"><a
                                        href="<c:url value='/edit/${storage.id}' />">Edit</a></td>

                                <td class="col-xs-1"><a href="<c:url value='/remove/${storage.id}' />">Delete</a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </c:if>

    </table>
    </table>
</div>

<div id="Coming" class="tabcontent">

    <br>
    <h3>Торговля и склад v.1.0.0</h3>
    <h3>Приход</h3>
    <p>Поступление материальных ценностей на склад.</p>
</div>

<div id="Reports" class="tabcontent">
    <br>
    <h3>Торговля и склад v.1.0.0</h3>
    <h3>Отчеты</h3>
    <p>Формирование документов строгой отчетности.</p>
    <p>Баг обнаружен когда пытался добавить материал ниже по скроллингу. Результат - после добавления
    таблицы пересобираются и теряется текущий селект на строке...скроллинг инициализируется в топ позицию...
    как вариант думаю, что нужно работать с ячейками таблицы, а не переформировывать строки...</p>
</div>
</body>


<link href="${contextPath}/resources/css/tabcontent.css" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css"
      integrity="2hfp1SzUoho7/TsGGGDaFdsuuDL0LX2hnUp6VkX3CUQ2K4K+xjboZdsXyp4oUHZj" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/js/bootstrap.min.js"
        integrity="VjEeINv9OSwtWFLAtmc4JCtEJXXBub00gtSnszmspDLCtC0I4z4nqz7rEFbIZLLU" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="${contextPath}/resources/js/fortabcontent.js"></script>

