function openEDO(event, plannerName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(plannerName).style.display = "block";
    event.currentTarget.className += " active";

    // remember current tab
    localStorage.setItem('activeTabId', plannerName);
    if (document.getElementsByTagName("input")[0].value) document.getElementById('BtnDirectory').click();
}

function goToEdit(val) {
    window.location = '/edit/' + val;
    document.getElementById('BtnDirectory').click();
    $(".selector").tab("destroy");
};
//////////////////////////////отключать курсор
$(document).ready(function () {
    $(".table").click(function () {
        $(this).val('X').css("cursor", "default");// set value and remove cursor
    });
});
//////////////////////////////////////////////////////////////////

window.onload = function () {
    var activeTabId = localStorage.getItem('activeTabId');
    if (activeTabId) {
        document.getElementById('Btn' + activeTabId).click();
    } else {
        document.getElementById('defaultOpen').click()
    }
    document.getElementById("thetable1").style.cursor = 'default';
    document.getElementById("thetable2").style.cursor = 'default';
    document.getElementById("thetableDict").style.cursor = 'default';
    redrawall1();
}
function redrawall1(){
    var arrayCategories = []
    for (var i = 1; i < document.getElementById("SelectMyLove").options.length; i++) {
        document.getElementById("SelectMyLove").options[i].count=0;
        arrayCategories.push(document.getElementById("SelectMyLove").options[i]);
    };
    $("#thetable2 tr").each(function (index) {

        if (index !== 0) {
            row = this;
            arrayCategories.forEach(function (item) {
                if (item.text == row.cells[4].firstChild.data){
                    item.count = parseInt(item.count)+parseInt(row.cells[2].firstChild.data);
                }
            });
        }
    });
    $("#thetableinfo tbody:last").empty();
    arrayCategories.forEach(function (item) {
        $("#thetableinfo tbody:last").last().append(" <tr>\n" +
            "<td class= 'col-xs-10'>" + item.text + "</td>\n" +
            "<td class= 'col-xs-2'>" + item.count + "</td>\n" +
            "</tr>\n");
    });
    document.getElementById('DivThetable2').style.visibility =(($("#thetable2 tr").closest("tr")).length==1)?'hidden':'visible';
    var countcomp=1;
    var flag = true;
    while (flag){
        arrayCategories.forEach(function (item) {
            if (parseInt(item.count) < countcomp){
                flag=false;
                console.log(item.text +" - "+item.count)
            }
        });
        (flag)?countcomp++:countcomp--;
    }
    $("#info1-sbor").text(" (" + countcomp+")");
}
function redrawall(data) {
    $('#thetable2').replaceWith($(data).find('#thetable2'));
    $('#thetable1').replaceWith($(data).find('#thetable1'));
    $('#thetableDict').replaceWith($(data).find('#thetableDict'));
    redrawall1();



}
$(document).on('click', '#thetable1 tbody tr', function (e) {
// если нажата строка а не Delete, тогда все делаем
    var modelAttr = $("#categories").val();
    //var modelAttr = ${categories};
    console.log(modelAttr);
    if (e.target.toString().indexOf('http://') != 0) {
        var flag = true;
//если таблица2 пустая то потом,после запроса, обновим страницу...чтоб нарисовалась
        $('#thetable2 tr').each(function () {
           // console.log('Пустотка')
            flag = false;
        });

        $.ajax({
            method: "post",
            url: "/addthetable1",
            data: {
                id: this.cells[0].firstChild.data,
                name: this.cells[1].firstChild.data,
                quantity: this.cells[2].firstChild.data,
                price: this.cells[3].firstChild.data,
                categoryId: this.cells[4].firstChild.data
            },
            success: function (data) {
redrawall(data);
            }
        });

        if (flag) {
            var div = document.getElementById('DivThetable2');
            div.style.visibility = 'visible';
            console.log("--"+div.style.visibility)
        };

    }


});
$(document).on('click', '#thetable2 tbody tr', function (e) {
// если нажата строка а не Delete, тогда все делаем
    if (e.target.toString().indexOf('http://') != 0) {
        var flag = true;
//если таблица2 пустая то потом,после запроса, обновим страницу...чтоб нарисовалась
        $('#thetable1 tr').each(function () {
            flag = false;
        });

        $.ajax({
            method: "post",
            url: "/addthetable2",
            data: {
                id: this.cells[0].firstChild.data,
                name: this.cells[1].firstChild.data,
                quantity: this.cells[2].firstChild.data,
                price: this.cells[3].firstChild.data,
                categoryId: this.cells[4].firstChild.data
            },
            success: function (data) {
redrawall(data);

            }
        });


        if (flag) {
            var div = document.getElementById('DivThetable2');
            div.style.visibility = 'hidden';
            console.log("--"+div.style.visibility)
        }
        ;
    }

});
$(document).on('mouseenter', '#thetable1 tbody tr', function () {
    mouseleaveColorIn(this);
});
$(document).on('mouseenter', '#thetableDict tbody tr', function () {
    mouseleaveColorIn(this);
    $(this).val('X').css("cursor", "default");
});

$(document).on('mouseenter', '#thetable2 tbody tr', function () {
    mouseleaveColorIn(this);
});

$(document).on('mouseleave', '#thetable2 tbody tr', function () {
    mouseleaveColorOut(this);
});
$(document).on('mouseleave', '#thetableDict tbody tr', function () {
    mouseleaveColorOut(this);
    $(this).val('X').css("cursor", "default");
});
//если фокуса нет на строках таблицы с комплектующими то инфо зануляем
$(document).on('mouseleave', '#thetable1 tbody tr', function () {
    mouseleaveColorOut(this)
});

function mouseleaveColorOut(boddy) {
    //////////// console.log($(boddy).parent().parent().attr('id'))
    for (var i = 0; i < 7; i++) boddy.cells[i].style.backgroundColor = ('#FFFFFF');
    if ($(boddy).parent().parent().attr('id') == 'thetable1') {
        $("#info1-item").hide();
        $("#info2-item").hide();
        $("#info3-item").hide();
    }
};

function mouseleaveColorIn(boddy) {
    // console.log($(boddy).parent().parent().attr('id'))
    for (var i = 0; i < 7; i++) boddy.cells[i].style.backgroundColor = "#c1c1c1";
    if ($(boddy).parent().parent().attr('id') == 'thetable1') {
        /////  console.log($(boddy).parent().parent('thetable1').index())
        //console.log($('#thetable1 >tbody >tr').length)
        if ($('#thetable1 >tbody >tr').length > 0) {
            $("#info1-item").text("Наименование комплектующего: " + boddy.cells[1].firstChild.data).show();
            $("#info2-item").text("Количество на складе: " + boddy.cells[2].firstChild.data).show();
            $("#info3-item").text("Цена: " + boddy.cells[3].firstChild.data).show();
        }
    }
};

function delete_user(row) {
    var $row = $(row).closest("tr");    // Find the row
    var $tds = $row.find("td");  //просматриваем всю строку
    $.each($tds, function () {
    });
    var value = $row[0].cells[0].firstChild.data;
    var valueIndexThetable = $row[0];
    var flag = true;
    $("#thetable1 tr").each(function (index) {
        if (index !== 0) {
            $row = $(this);
            var id = $row.find("td:first").text();
            if (id.indexOf(value) == 0) {
                flag = false;
                if (valueIndexThetable.cells[2].firstChild.data > 0) {
                    valueIndexThetable.cells[2].firstChild.data--;
                    this.cells[2].firstChild.data++;
                }
            }
        }
    });
}
