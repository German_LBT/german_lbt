package com.german.spring.service;

import com.german.spring.dao.CategoryDAO;
import com.german.spring.model.Category;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * @author Dinesh Rajput
 *
 */
//@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
@Service
public class CategoryServiceImpl implements CategoryService {


    private CategoryDAO categoryDAO;

    public void setCategoryDAO(CategoryDAO categoryDAO) {
        this.categoryDAO = categoryDAO;
    }

    @Override
    @Transactional
    public void addCategory(Category p) {
        this.categoryDAO.addCategory(p);
    }

    @Override
    @Transactional
    public List<Category> getCategoriesList() {
        return this.categoryDAO.getCategoriesList();
    }

    @Override
    @Transactional
    public void updateCategory(Category p) {
        this.categoryDAO.updateCategory(p);
    }

    @Override
    @Transactional
    public Category getCategory(int categoryId) {
        return this.categoryDAO.getCategory(categoryId);
    }

    @Override
    @Transactional
    public void deleteCategory(int categoryId) {
        this.categoryDAO.deleteCategory(categoryId);
    }

}
