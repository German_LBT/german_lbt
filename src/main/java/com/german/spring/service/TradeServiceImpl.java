package com.german.spring.service;

import java.util.List;

import com.german.spring.dao.TradeDAO;
import com.german.spring.model.Storage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TradeServiceImpl implements TradeService {
	
	private TradeDAO tradeDAO;

	public void setTradeDAO(TradeDAO tradeDAO) {
		this.tradeDAO = tradeDAO;
	}

	@Override
	@Transactional
	public void addTrader(Storage p) {
		this.tradeDAO.addTrader(p);
	}

	@Override
	@Transactional
	public void updateTrader(Storage p) {
		this.tradeDAO.updateTrader(p);
	}

	@Override
	@Transactional
	public List<Storage> listTraders() {
		return this.tradeDAO.listTraders();
	}

	@Override
	@Transactional
	public Storage getTraderById(int id) {
		return this.tradeDAO.getTraderById(id);
	}

	@Override
	@Transactional
	public void removeTrader(int id) {
		this.tradeDAO.removeTrader(id);
	}

}
