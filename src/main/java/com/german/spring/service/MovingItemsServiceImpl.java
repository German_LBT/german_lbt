package com.german.spring.service;

import com.german.spring.dao.MovingDAO;
import com.german.spring.model.Assembly;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class MovingItemsServiceImpl implements MovingItemsService {
    private MovingDAO movingDAO;

    public void setMovingDAO(MovingDAO movingDAO) {
        this.movingDAO = movingDAO;
    }

    @Override
    @Transactional
    public void addMoving(Assembly p) {
this.movingDAO.addMoving(p);
    }

    @Override
    @Transactional
    public void updateMoving(Assembly p) {
this.movingDAO.updateMoving(p);
    }

    @Override
    @Transactional
    public List<Assembly> listMoving() {

        return this.movingDAO.listMoving();
    }

    @Override
    @Transactional
    public Assembly getMovingById(int id) {
        System.out.println(id);
        return this.movingDAO.getMovingById(id);
    }

    @Override
    @Transactional
    public void removeMoving(int id) {
this.movingDAO.removeMoving(id);
    }
}
