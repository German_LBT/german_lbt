package com.german.spring.service;

import com.german.spring.model.Category;

import java.util.List;



public interface CategoryService {

    public void addCategory(Category category);

    public List<Category> getCategoriesList();
    public void updateCategory(Category category);
    public Category getCategory(int categoryId);

    public void deleteCategory(int categoryId);

}

