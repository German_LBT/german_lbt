package com.german.spring.service;

import com.german.spring.model.Assembly;


import java.util.List;

public interface MovingItemsService {
    public void addMoving(Assembly p);
    public void updateMoving(Assembly p);
    public List<Assembly> listMoving();
    public Assembly getMovingById(int id);
    public void removeMoving(int id);
}
