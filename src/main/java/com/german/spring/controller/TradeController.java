package com.german.spring.controller;

import com.german.spring.model.Assembly;
import com.german.spring.model.Category;
import com.german.spring.model.Storage;
import com.german.spring.service.CategoryService;
import com.german.spring.service.MovingItemsService;
import com.german.spring.service.TradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class TradeController {

    private TradeService tradeService;
    private MovingItemsService movingItemsService;
    private CategoryService categoryService;

    @Autowired(required = true)
    @Qualifier(value = "traderService")
    public void setTradeService(TradeService ps) {
        this.tradeService = ps;
    }

    @Autowired(required = true)
    @Qualifier(value = "movingItemsService")
    public void setMovingItemsService(MovingItemsService ps) {
        this.movingItemsService = ps;
    }


    @Autowired(required = true)
    @Qualifier(value = "categoryService")
    public void setCategoryService(CategoryService ps) {
        this.categoryService = ps;
    }


    @RequestMapping(value = {"/storage", "/"}, method = RequestMethod.GET)
    public String listTraders(Model model) {
        addModels(model);
        return "storage";
    }



    //For add and update trader both
    @RequestMapping(value = "/storage/add", method = RequestMethod.POST)
    public String addTrader(@ModelAttribute("storage") Storage p) {
        System.out.println("aaaaaaaaa/storage/add   aaaaaaaaaaaa");
        if (p.getId() == 0) {
            this.tradeService.addTrader(p);
        } else {

            this.tradeService.updateTrader(p);
        }

        return "redirect:/storage";
    }
    @RequestMapping(value = "/addthetable1", method = RequestMethod.POST)
    public String getDrug(@ModelAttribute("assembly") Assembly p) {
        boolean flag = true;
        Storage storage = this.tradeService.getTraderById(p.getId());
        if (storage.getQuantity() > 0) {
            storage.setQuantity(storage.getQuantity() - 1);
            p.setCategory(storage.getCategory());
            this.tradeService.updateTrader(storage);
            for (Assembly assembly : this.movingItemsService.listMoving()) {
                if (assembly.getId() == p.getId()) {
                    flag = false;
                    p.setQuantity(assembly.getQuantity() + 1);
                    this.movingItemsService.updateMoving(p);
                }
            }
            if (flag) {
                p.setQuantity(1);
                this.movingItemsService.addMoving(p);
            }
            Map<String, Object> model = new HashMap<String, Object>();
            model.put("listMoving", this.movingItemsService.listMoving());
        }
        return "redirect:/";

    }
    @RequestMapping(value = "/addthetable2", method = RequestMethod.POST)
    public String getDrug(@ModelAttribute("storage") Storage p) {
        boolean flag = true;
        Assembly assembly = this.movingItemsService.getMovingById(p.getId());
        p.setCategory(assembly.getCategory());
        if (assembly.getQuantity() > 1) {
            assembly.setQuantity(assembly.getQuantity() - 1);
            this.movingItemsService.updateMoving(assembly);
            flag = false;
        }
            for (Storage storage : this.tradeService.listTraders()) {
                if (storage.getId() == p.getId()) {
                    p.setQuantity(storage.getQuantity() + 1);
                    this.tradeService.updateTrader(p);
                }
            }
            if (flag) this.movingItemsService.removeMoving(p.getId());

        return "redirect:/";

    }

        @RequestMapping("/remove/{id}")
        public String removeTrader ( @PathVariable("id") int id){
            this.tradeService.removeTrader(id);
            return "redirect:/storage";
        }

        @RequestMapping("/edit/{id}")
        public String editTrader ( @PathVariable("id") int id, Model model){
            System.out.println("aaaaaaaaaaeditTraderaaaaaaaaaaaaa");
            model.addAttribute("storage", this.tradeService.getTraderById(id));
            model.addAttribute("listTraders", this.tradeService.listTraders());
            model.addAttribute("category",new Category());
            model.addAttribute("categories", (this.categoryService.getCategoriesList()));
            return "storage";
        }



    @RequestMapping(value = "/saveCategory", method = RequestMethod.POST)
    public ModelAndView saveEmployee(@ModelAttribute("storage") Category category,
                                     BindingResult result) {
        System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

        categoryService.addCategory(category);
        return new ModelAndView("redirect:/storage");
    }

    @RequestMapping(value = "/addCategory", method = RequestMethod.POST)
    public String addCategory(@ModelAttribute("category") Category p, BindingResult result, Model model) {
        if (p.getCategoryId() == 0) this.categoryService.addCategory(p);
         else this.categoryService.updateCategory(p);
        addModels(model);
        return "redirect:/storage";
    }

    @RequestMapping(value = "/deleteCategory/{id}")
    public String deleteCategory( @PathVariable("id") int id) {
        categoryService.deleteCategory(id);
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("categories",  categoryService.getCategoriesList());
        return "redirect:/storage";
    }

    @RequestMapping(value = "editCategory/{id}")
    public String editCategory1 ( @PathVariable("id") int id, Model model) {
        System.out.println(id);
        System.out.println(model);
        model.addAttribute("storage", new Storage());
        model.addAttribute("listTraders", this.tradeService.listTraders());
        model.addAttribute("assembly", new Assembly());
        model.addAttribute("listMoving", this.movingItemsService.listMoving());
        model.addAttribute("category", this.categoryService.getCategory(id));
        model.addAttribute("categories", this.categoryService.getCategoriesList());
        return "storage";
    }


        @RequestMapping(value="/categories", method = RequestMethod.GET)
    public List<Category> getCategoriesList() {
        return categoryService.getCategoriesList();
    }

    private void addModels(Model model){
        model.addAttribute("category",new Category());
        model.addAttribute("categories", (this.categoryService.getCategoriesList()));
        model.addAttribute("storage", new Storage());
        model.addAttribute("listTraders", this.tradeService.listTraders());
        model.addAttribute("assembly", new Assembly());
        model.addAttribute("listMoving", this.movingItemsService.listMoving());
    }

}