package com.german.spring.model;

import javax.persistence.*;

/**
 * Entity bean with JPA annotations
 * Hibernate provides JPA implementation
 * @author pankaj
 *
 */
@Entity
@Table(name="STORAGE")
public class Storage {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String name;
	private int quantity;
	private int price;
	boolean importance;
	@OneToOne
	@JoinColumn(name="categoryId")
	private Category category;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getPrice() {return price;}

	public void setPrice(int price) {this.price = price;}

	public boolean isImportance() {return importance;}

	public void setImportance(boolean importance) {this.importance = importance;}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}

	@Override
	public String toString(){
		return "id="+id+", name="+name+", quantity="+quantity+", price="+price+",importance="+importance;
	}
}
