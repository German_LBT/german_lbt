package com.german.spring.dao;

import com.german.spring.model.Assembly;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MovingDAOImpl implements MovingDAO{

    private static final Logger logger = LoggerFactory.getLogger(MovingDAOImpl.class);

    private SessionFactory sessionFactory;


    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }
    @Override
    public void addMoving(Assembly p) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(p);
        logger.info("Assembly saved successfully, Assembly Details="+p);
    }

    @Override
    public void updateMoving(Assembly p) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(p);
        logger.info("Assembly updated successfully, Assembly Details="+p);
    }

    @Override
    public List<Assembly> listMoving() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Assembly> assemblyList = session.createQuery("from Assembly").list();
        for (Assembly p : assemblyList) {
            logger.info("Assembly List::" + p);
        }
        return assemblyList;
    }

    @Override
    public Assembly getMovingById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Assembly p = (Assembly) session.load(Assembly.class, new Integer(id));
        logger.info("Assembly loaded successfully, Assembly details="+p);
        return p;
    }

    @Override
    public void removeMoving(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Assembly p = (Assembly) session.load(Assembly.class, new Integer(id));
        if(null != p){
            session.delete(p);
        }
        logger.info("Assembly deleted successfully, Assembly details="+p);
    }
}
