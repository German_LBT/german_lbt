package com.german.spring.dao;

import java.util.List;

import com.german.spring.model.Category;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;


@Repository
public class CategoryDAOImpl implements CategoryDAO {

    private SessionFactory sessionFactory;
    private static final Logger logger = LoggerFactory.getLogger(CategoryDAOImpl.class);

    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory = sf;
    }

    @Override
    public void addCategory(Category p) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(p);
        logger.info("Category saved successfully, Category Details="+p);
    }

    @Override
    public void updateCategory(Category p) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(p);
        logger.info("Category updated successfully, Category Details="+p);
    }


    @Override
    public List<Category> getCategoriesList() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Category> categoryList =session.createQuery("from Category").list();
        for (Category p : categoryList) {
            logger.info("Category List::" + p);
        }
        return categoryList;
    }

    @Override
    public Category getCategory(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Category p = (Category) session.load(Category.class, new Integer(id));
        logger.info("Category loaded successfully, Category details="+p);
        return p;
    }

    @Override
    public void deleteCategory(int categoryId) {
        Session session = this.sessionFactory.getCurrentSession();
        Category p = (Category) session.load(Category.class, new Integer(categoryId));
        if(null != p) session.delete(p);

        logger.info("Category deleted successfully, Category details="+p);
    }

}
