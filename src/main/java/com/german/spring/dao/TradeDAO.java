package com.german.spring.dao;

import java.util.List;

import com.german.spring.model.Storage;

public interface TradeDAO {

	public void addTrader(Storage p);
	public void updateTrader(Storage p);
	public List<Storage> listTraders();
	public Storage getTraderById(int id);
	public void removeTrader(int id);
}
