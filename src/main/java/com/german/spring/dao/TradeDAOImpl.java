package com.german.spring.dao;

import java.util.List;

import com.german.spring.model.Storage;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class TradeDAOImpl implements TradeDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(TradeDAOImpl.class);

	private SessionFactory sessionFactory;


	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public void addTrader(Storage p) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(p);
		logger.info("Storage saved successfully, Storage Details="+p);
	}

	@Override
	public void updateTrader(Storage p) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(p);
		logger.info("Storage updated successfully, Storage Details="+p);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Storage> listTraders() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Storage> tradersList = this.sessionFactory.getCurrentSession().createQuery("from Storage").list();
		for(Storage p : tradersList){
			logger.info("Storage List::"+p);
		}
		return tradersList;
	}

	@Override
	public Storage getTraderById(int id) {
		Session session = this.sessionFactory.getCurrentSession();		
		Storage p = (Storage) session.load(Storage.class, new Integer(id));
		logger.info("Storage loaded successfully, Storage details="+p);
		return p;
	}

	@Override
	public void removeTrader(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Storage p = (Storage) session.load(Storage.class, new Integer(id));
		if(null != p){
			session.delete(p);
		}
		logger.info("Storage deleted successfully, trader details="+p);
	}

}
