package com.german.spring.dao;
import com.german.spring.model.Category;

import java.util.List;

public interface CategoryDAO {

    public void addCategory(Category category);

    public void updateCategory(Category category);

    public List<Category> getCategoriesList();

    public Category getCategory(int categoryId);

    public void deleteCategory(int categoryId);
}
