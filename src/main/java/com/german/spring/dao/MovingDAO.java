package com.german.spring.dao;

import com.german.spring.model.Assembly;

import java.util.List;

public interface MovingDAO {
    public void addMoving(Assembly p);
    public void updateMoving(Assembly p);
    public List<Assembly> listMoving();
    public Assembly getMovingById(int id);
    public void removeMoving(int id);
}
