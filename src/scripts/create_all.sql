-- auto-generated definition
CREATE TABLE `category` (
  `categoryId` int(11) NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`categoryId`)
)
  collate = utf8mb4_unicode_ci;
create table storage
(
  id         int(11) unsigned auto_increment
    primary key,
  name       varchar(50) default '' not null,
  quantity   int                    null,
  price      int                    null,
  importance tinyint(1)             null,
  categoryId int(11) DEFAULT NULL,
  KEY `FK23254A0CAF274936` (`categoryId`),
  CONSTRAINT `FK23254A0CAF274936` FOREIGN KEY (`categoryId`) REFERENCES `category` (`categoryId`)
)
  collate = utf8mb4_unicode_ci;

create table assembly
(
  id         int(11)                unsigned,
  name       varchar(50) default '' not null,
  quantity   int                    null,
  price      int                    null,
  importance tinyint(1)             null,
  categoryId int(11) DEFAULT NULL,
  KEY `FK23254A0CAF274937` (`categoryId`),
  CONSTRAINT `FK23254A0CAF274937` FOREIGN KEY (`categoryId`) REFERENCES `category` (`categoryId`)
)
  collate = utf8mb4_unicode_ci;

