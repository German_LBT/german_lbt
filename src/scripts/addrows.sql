INSERT INTO `tradedb`.`category` (`categoryId`, `categoryName`) VALUES (1, 'Дополнительное');
INSERT INTO `tradedb`.`category` (`categoryId`, `categoryName`) VALUES (2, 'Материнская карта');
INSERT INTO `tradedb`.`category` (`categoryId`, `categoryName`) VALUES (3, 'Процессор');
INSERT INTO `tradedb`.`category` (`categoryId`, `categoryName`) VALUES (4, 'Кулер для процессора');
INSERT INTO `tradedb`.`category` (`categoryId`, `categoryName`) VALUES (5, 'Оперативная память');
INSERT INTO `tradedb`.`category` (`categoryId`, `categoryName`) VALUES (6, 'Жесткий диск');
INSERT INTO `tradedb`.`category` (`categoryId`, `categoryName`) VALUES (7, 'Корус с Б/П');
INSERT INTO `tradedb`.`category` (`categoryId`, `categoryName`) VALUES (8, 'Видеокарта');
INSERT INTO `tradedb`.`category` (`categoryId`, `categoryName`) VALUES (9, 'Монитор');
INSERT INTO `tradedb`.`category` (`categoryId`, `categoryName`) VALUES (10, 'Клавиатура');
INSERT INTO `tradedb`.`category` (`categoryId`, `categoryName`) VALUES (11, 'Мышь');
INSERT INTO `tradedb`.`storage` (`id`, `name`, `quantity`, `price`, `importance`, `categoryId`) VALUES (1, 'Материнская плата "Asus"', 25, 3800, 1, 2)
INSERT INTO `tradedb`.`storage` (`id`, `name`, `quantity`, `price`, `importance`, `categoryId`) VALUES (2, 'Материнская плата "MSI"', 25, 3500, 1, 2)
INSERT INTO `tradedb`.`storage` (`id`, `name`, `quantity`, `price`, `importance`, `categoryId`) VALUES (3, 'Процессор "i3" x-64', 25, 5380, 1, 3)
INSERT INTO `tradedb`.`storage` (`id`, `name`, `quantity`, `price`, `importance`, `categoryId`) VALUES (4, 'Процессор "i5" x-64', 25, 6120, 1, 3)
INSERT INTO `tradedb`.`storage` (`id`, `name`, `quantity`, `price`, `importance`, `categoryId`) VALUES (5, 'Вентилятор для процессора "Titan"', 25, 320, 1, 4)
INSERT INTO `tradedb`.`storage` (`id`, `name`, `quantity`, `price`, `importance`, `categoryId`) VALUES (6, 'Вентилятор для процессора "Zalman"', 25, 670, 1, 4)
INSERT INTO `tradedb`.`storage` (`id`, `name`, `quantity`, `price`, `importance`, `categoryId`) VALUES (7, 'DDR-4 8Gb "Samsung"', 25, 2900, 1, 5)
INSERT INTO `tradedb`.`storage` (`id`, `name`, `quantity`, `price`, `importance`, `categoryId`) VALUES (8, 'DDR-4 4Gb "Corsair"', 25, 2140, 1, 5)
INSERT INTO `tradedb`.`storage` (`id`, `name`, `quantity`, `price`, `importance`, `categoryId`) VALUES (9, 'HDD 750Gb "Toshiba"', 25, 4820, 1, 6)
INSERT INTO `tradedb`.`storage` (`id`, `name`, `quantity`, `price`, `importance`, `categoryId`) VALUES (10, 'HDD 1TB "Seagate"', 25, 5070, 1, 6)
INSERT INTO `tradedb`.`storage` (`id`, `name`, `quantity`, `price`, `importance`, `categoryId`) VALUES (11, 'Minitower "Thermaltake" 2000Bt', 25, 3900, 1, 7)
INSERT INTO `tradedb`.`storage` (`id`, `name`, `quantity`, `price`, `importance`, `categoryId`) VALUES (12, 'BigTower "Zalman" 2500Bt', 25, 4400, 1, 7)
INSERT INTO `tradedb`.`storage` (`id`, `name`, `quantity`, `price`, `importance`, `categoryId`) VALUES (13, 'Video Card 2GB "Radeon"', 25, 8500, 1, 8)
INSERT INTO `tradedb`.`storage` (`id`, `name`, `quantity`, `price`, `importance`, `categoryId`) VALUES (14, 'Video Card 4GB "MSI"', 25, 9250, 1, 8)
INSERT INTO `tradedb`.`storage` (`id`, `name`, `quantity`, `price`, `importance`, `categoryId`) VALUES (15, 'Samsung 24'' Black flat', 25, 17510, 1, 9)
INSERT INTO `tradedb`.`storage` (`id`, `name`, `quantity`, `price`, `importance`, `categoryId`) VALUES (16, 'Dell 27'' Silver DVI', 25, 19000, 1, 9)
INSERT INTO `tradedb`.`storage` (`id`, `name`, `quantity`, `price`, `importance`, `categoryId`) VALUES (17, 'Logitech "G103" ergonomic', 25, 990, 1, 10)
INSERT INTO `tradedb`.`storage` (`id`, `name`, `quantity`, `price`, `importance`, `categoryId`) VALUES (18, 'SVEN "Derevko" White USB', 25, 370, 1, 10)
INSERT INTO `tradedb`.`storage` (`id`, `name`, `quantity`, `price`, `importance`, `categoryId`) VALUES (19, 'Mouse "Logitech"', 25, 400, 1, 11)
INSERT INTO `tradedb`.`storage` (`id`, `name`, `quantity`, `price`, `importance`, `categoryId`) VALUES (20, 'Mouse "SVEN"', 25, 250, 1, 11)
INSERT INTO `tradedb`.`storage` (`id`, `name`, `quantity`, `price`, `importance`, `categoryId`) VALUES (21, 'Коврик для мышек BIG', 50, 50, 1, 1)
INSERT INTO `tradedb`.`storage` (`id`, `name`, `quantity`, `price`, `importance`, `categoryId`) VALUES (22, 'Коврик для тапочек STRONG', 50, 65, 1, 1)